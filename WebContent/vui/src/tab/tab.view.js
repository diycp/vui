;(function(){

/**
 * @ignore
 * @class
 */
VUI.Class('TabView',{
	/**
	 * 
	 * @constructor
	 */
	init:function(cmp) {
		this._super(cmp);
		var id = cmp.id;
		this.selectedClass = 'ui-tabs-selected ui-state-active';
		this.disabledClass = 'ui-state-disabled';
		
		this.v_tabnav = this.buildDirectiveName('tabnav');
		this.v_tabview = this.buildDirectiveName('tabview');
	}
	,initDirective:function() {
		var that = this;
		
		this.directive(this.v_tabview,function(value,$div){
			$div.width(that.opt('width'));
			$div.height(that.opt('height'));
			if(!that.opt('border')) {
				$div.css('border','0px');
			}
		});
		
		this.directive(this.v_tabnav,function(value,$li){
			var index = value.index;
			var $div = that.getTabCont();
			
			var item = that.getItemByIndex(index);
			that.appendContent($div,item);
		});
		
	}
	,getItemByIndex:function(index) {
		var item = this.opt('items')[index];
		this.formatItem(item);
		return item;
	}
	,formatItem:function(item) {
		this.formatTitle(item);
		this.formatContentId(item);
		this.formatSize(item);
	}
	,appendContent:function($div,item) {
		this.formatItem(item);
		this.appendDiv($div,item);
	}
	,formatSize:function(item) {
		item.width = item.width || 'auto';
		item.height = item.height || 'auto';
	}
	,formatTitle:function(item) {
		item.title = item.title || "New Tab";
	}
	,formatContentId:function(item) {
		if(item.content&&!item.contentId) {
			item.contentId = 'tab_cont_id' + VUI.getId();
			$('body').append($('<div id="'+item.contentId+'"></div>').html(item.content));
		}
		return item;
	}
	,appendDiv:function($div,item) {
		if(!item.contentId) {
			return;
		}
		var $content = $('#' + item.contentId);
		// 没有渲染过,解决多个item具有相同contentId
		if(!$content.data('appended')) {
			$content.addClass('pui-tabview-panel ui-widget-content ui-corner-bottom')
				.width(item.width)
				.height(item.height)
				.data('appended',true)
				.hide();
			
			$div.append($content); // 必须先加载到html中,不然$content.siblings()会有问题
			
			if(item.selected) {
				$content.show();
				$content.siblings().hide();
			}
		}
		
	}
	,getItem:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		if($a.length == 0) {
			return null;
		}
		var items = this.opt('items');
		var index = this.getIndex($a);
		return items[index];
	}
	,getTabIndex:function(item) {
		var $a = this.get$A(item.title);
		return this.getIndex($a);;
	}
	,getSelected:function() {
		var $tab = this.getTab();
		var $li = $tab.find('.pui-tabview-nav .ui-tabs-selected');
		var index = this.getIndex($li);
		
		return this.opt('items')[index];
	}
	,get$A:function(titleOrIndex) {
		var $tab = this.getTab();
		return $tab.find('.pui-tabview-nav').find('a[tab-index="'+titleOrIndex+'"],a[tab-title="'+titleOrIndex+'"]').eq(0);
	}
	,hideItem:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		var $li = $a.parent();
		var index = this.getIndex($li);
		var panelId = $li.attr('panel-id');
		$li.hide();
		$('#'+panelId).hide();
		
		this.selectNext(index);
	}
	,showItem:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		var $li = $a.parent();
		var panelId = $li.attr('panel-id');
		$li.show();
	}
	,getIndex:function($el) {
		return parseInt($el.attr('tab-index'));
	}
	,selectNext:function(index) {
		var items = this.opt('items');
		if(this.isSelected(index)) {
			var nextSelectIndex = 0;
			if(items.length > 1){
				nextSelectIndex = index > 0 ? index - 1 : index + 1;
			}
			this.select(nextSelectIndex);
		}
	}
	,select:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		this._select($a);
	}
	,exists:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		return $a.length > 0;
	}
	,ngClick:function(e) {
		var $target = $(e.target);
		this._select($target);
	}
	,close:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		var index = this.getIndex($a);
		
		this.doClose(index);
	}
	,isSelected:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		return $a.parent().hasClass('ui-state-active');
	}
	,doClose:function(index) {
		index = parseInt(index);
		var items = this.opt('items');
		var item = items[index]
		var param = {item:item,index:index};
		var ret = this.cmp.fire('BeforeClose',param);
		
		if(ret === false) {
			return;
		}
		
		this.selectNext(index);
		
		var contnetId = item.contentId;
		
		items.splice(index, 1);
		$('#'+contnetId).remove();
		
		this.cmp.fire('Close',param);
	}
	,enableTab:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		var $li = $a.parent();
		$li.removeClass(this.disabledClass);
	}
	,disableTab:function(titleOrIndex) {
		var $a = this.get$A(titleOrIndex);
		var $li = $a.parent();
		if(!$li.hasClass(this.disabledClass)) {
			$li.removeClass(this.selectedClass).addClass(this.disabledClass);
		}
	}
	,_select:function($target) {
		var panelId = $target.attr('panel-id');
		var $li = $target.parent();
		
		if($li.hasClass('ui-state-disabled')) {
			return;
		}
		$li.addClass(this.selectedClass).siblings().removeClass(this.selectedClass);
		$li.attr('tab-selected',true).siblings().attr('tab-selected',false);
		
		if(panelId) {
			$('#'+panelId).show().siblings().hide();
		}
		
		var index = this.getIndex($target);
		var item = this.getItem(index);
		var param = {item:item,index:index};
		this.cmp.fire('Select',param);
	}
	,getUniqueContClass:function() {
		return this.getUniqueClass('ajtabcont');
	}
	,getUniqueTabClass:function() {
		return this.getUniqueClass('ajtab');
	}
	,getTab:function() {
		return $('.'+this.getUniqueTabClass()).eq(0);
	}
	,getTabCont:function() {
		return $('.'+this.getUniqueContClass()).eq(0);
	}
	//@override
	,getTemplate:function(){
		var template = 
		'<div v-'+this.v_tabview+'="" class="'+this.getUniqueTabClass()+' ui-tabs pui-tabview ui-widget ui-widget-content ui-corner-all ui-hidden-container pui-tabview-top">' +
			'<ul class="ui-tabs-nav pui-tabview-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">' +
				'<li v-'+this.v_tabnav+'="{index:index}"' +
						'v-for="(index, item) in cmp.opts.items" ' +
						'tab-index="{{index}}" panel-id="{{item.contentId}}" ' +
						'class="vui-hover ui-state-default ui-corner-top " ' +
						':class="item.selected ? \''+this.selectedClass+'\':\'\'" ' +
						'>' +
						
					'<a @click="cmp.view.ngClick($event)" panel-id="{{item.contentId}}" tab-index="{{index}}" tab-title="{{item.title}}" href="javascript:void(0);">{{item.title}}</a>' +
					'<span v-show="item.closable" ' +
						'@click.self="cmp.view.doClose(index)"' + 
						'tab-index="{{index}}" ' +
						'class="ui-icon ui-icon-close"></span>' + 
					
				'</li>' + 
			'</ul>' + 
			'<div class="'+ this.getUniqueContClass() +' pui-tabview-panels">' +
			'</div>' +
		'</div>';
		return template;
	}
},VUI.View);

})();