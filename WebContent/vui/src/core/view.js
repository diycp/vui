/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */
;(function(){


var hoverInited = false;

/**
 * 所有视图类的父类
 * @ignore
 * @class
 */
VUI.Class('View',{
	/**
	 * 构造函数
	 * @ignore
	 */
	init:function(cmp) {
		this.cmp = cmp;
		
		this.$wraper = this.createOuter();
		
		this.initHover();
	}
	// 构建指令名称
	,buildDirectiveName:function(name) {
		return name + this.cmp.id;
	}
	,createOuter:function() {
		var $wraper = $('<div></div>')
			.attr('id',this.cmp.getAppName())
			.addClass('vui-wraper')
			.css({position:this.opt('position')});
		
		if(this.opt('zindex')) {
			$wraper.css({'z-index':this.opt('zindex')});
		}
		
		if(this.opt('autoHide')) {
			$wraper.hide();
		}
		
		return $wraper;
	}
	,getTemplate:function() {
		alert('类' + this.getClassName() + '必须重写View.getTemplate()');
	}
	,render:function(dom) {
		var $wraper = this.getWraper();
		var renderDom = dom || this.getRenderDom();
		
		$wraper.html(this.getTemplate());
		
		$(renderDom).append($wraper);
		
		this.afterRender();
		
		this.initDirective();
	}
	,getRenderDom:function() {
		var renderId = this.opt('renderId');
		return renderId && typeof renderId === 'string'
			? document.getElementById(renderId) 
			: document.body;
	}
	,getWraper:function() {
		return this.$wraper;
	}
	,opt:function(optName,val) {
		return this.cmp.opt(optName,val);
	}
	,afterRender:function() {
		
	}
	,initHover:function() {
		if(!hoverInited) {
			var hoverClassName = 'ui-state-hover';
			// ui-widget-content
			// .vui-hover,.pui-button,.pui-dialog-titlebar-icon
			$(document).on('mouseover','.vui-hover,.pui-button,.pui-dialog-titlebar-icon',function(){
				var el = $(this);
				if(el.hasClass('ui-state-disabled')) {
					return;
				}
				el.addClass(hoverClassName);
			}).on('mouseout','.vui-hover,.pui-button,.pui-dialog-titlebar-icon',function(){
				$(this).removeClass(hoverClassName);
			});
			
			hoverInited = true;			
		}
	}
	,initDirective:function(){
		
	}
	,show:function() {
		this.getWraper().show();
	}
	,hide:function() {
		this.getWraper().hide();
	}
	,directive:function(name,bindHandler) {
		Vue.directive(name, function (value) {
			bindHandler.call(this,value,$(this.el));
		})
	}
	,getUniqueClass:function(className) {
		return className + this.cmp.id;
	}
	/**
	 * 添加hover效果,FDLib.addHoverEffect(dom);
	 */
	,addHoverEffect:function($dom,hoverClassName) {
		hoverClassName = hoverClassName || 'ui-state-hover';
		$dom.mouseover(function(){
			$dom.addClass(hoverClassName);
		}).mouseout(function(){
			$dom.removeClass(hoverClassName);
		});
	}
});

})();